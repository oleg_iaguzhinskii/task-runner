export const LOGIN_PENDING = 'LOGIN_PENDING';
export const LOGIN_SUCCESS = 'LOGIN SUCCESS';
export const LOGIN_ERROR = 'LOGIN ERROR';
export const LOG_OUT = 'LOG_OUT';
export const INVALID_LOGIN_TEXT = 'INVALID_LOGIN_TEXT';
export const VALID_LOGIN_TEXT = 'VALID_LOGIN_TEXT';