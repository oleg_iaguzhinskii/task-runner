import React, { Component } from 'react'
import { LinkContainer, IndexLinkContainer } from 'react-router-bootstrap'
import styles from '../../styles/components/navigation.css'
import { Redirect } from 'react-router'

import { PageHeader, Nav, Navbar, NavItem, NavDropdown, MenuItem } from 'react-bootstrap'

const NAV_ROUTE_ITEMS = [
    {
        route: '/',
        name: 'Общие задачи'
    },
    {
        route: '/my_tasks',
        name: 'Мои задачи'
    },
    {
        route: '/create_task',
        name: 'Создать'
    },
    {
        route: '/create_category',
        name: 'Категории'
    }
];

export default class Navigation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: false
        };
    }

    onClick() {
        const { setOpenedPage, logout } = this.props;
        setOpenedPage('/');
        logout();
        this.setState({redirect: true});
    }

    navigationBuilder(isLogged) {
        const { setOpenedPage, page } = this.props;

        return (
            <Nav activeKey={1}>
                {NAV_ROUTE_ITEMS.map(function(item, i) {
                    const { route, name } = item;
                    return (
                        <IndexLinkContainer to={route} key={i}>
                            <NavItem eventKey={i} disabled={!isLogged && route !== '/'}
                                     onClick={() => {
                                        if (page !== route) {
                                            setOpenedPage(route);
                                        }
                                     }}>{name}</NavItem>
                        </IndexLinkContainer>
                    )
                })}
            </Nav>
        );
    }

    render() {
        const { loggedUserLogin, isLogged, setOpenedPage, page } = this.props;
        const { redirect } = this.state;

        const nickname = isLogged ? (
            <div className={styles.nickname}>
                {/*<span className={styles.circle} data-title='Пора выполнить задание!'/>*/}
                {'привет, ' + loggedUserLogin[0].toUpperCase() + loggedUserLogin.slice(1) + '!'}
            </div>
        ) : null;

        const loginButton = isLogged ? (
            <Nav pullRight>
                <NavDropdown eventKey={5} title="Прочее" id="basic-nav-dropdown">
                    <LinkContainer to='/my_page' onClick={() => setOpenedPage('/my_page')}>
                        <MenuItem eventKey={5.1}>Личный&nbsp;кабинет</MenuItem>
                    </LinkContainer>
                    <LinkContainer to='/settings' onClick={() => setOpenedPage('/settings')}>
                        <MenuItem eventKey={5.2}>Настройки</MenuItem>
                    </LinkContainer>
                    <MenuItem divider />
                    <MenuItem eventKey={5.3} onClick={::this.onClick}>Выход</MenuItem>
                </NavDropdown>
            </Nav>
        ) : (
            <Nav pullRight>
                <LinkContainer to='/login'
                               style={page === '/login' ? {color: 'white', textDecoration: 'none'}
                                : {color: 'inherit', textDecoration: 'none'}}
                               onClick={(e) => {
                                    if (page === '/login') {
                                        e.preventDefault();
                                        return;
                                    }
                                    setOpenedPage('/login')
                                }}>
                    <NavItem eventKey={5}>Вход</NavItem>
                </LinkContainer>
                {redirect ? <Redirect to='/'/> : null}
            </Nav>
        );

        return (
            <div>
                <div className={styles.header}>
                    <div className={styles.title}>
                        <PageHeader style={{display: 'inline-block'}}>Task Runner</PageHeader>
                        {nickname}
                    </div>
                </div>

                <div className={styles.navSize}><Navbar inverse collapseOnSelect>
                    <Navbar.Header>
                        <Navbar.Toggle />
                    </Navbar.Header>
                    <Navbar.Collapse>
                        {::this.navigationBuilder(isLogged)}
                        {loginButton}
                    </Navbar.Collapse>
                </Navbar></div>
            </div>
        )
    }
}
