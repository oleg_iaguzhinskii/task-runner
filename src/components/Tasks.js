import React, { Component } from 'react'
import styles from '../../styles/components/tasks.css'
import Img from 'react-image';
import { Col } from 'react-bootstrap'

export default class Tasks extends Component {
    render() {
        const { tasksList, page, openModalWindow, setIdOfEditTask, deleteTheTask, doesShow } = this.props;

        const tasks = tasksList ? (
            <div>
                {tasksList.map(function(item) {
                    let color = '';
                    switch (item.tag) {
                        case 1: color = 'red';
                        break;
                        case 2: color = 'green';
                        break;
                        case 3: color = 'yellow';
                        break;
                        default: color = 'inherit';
                    }
                    return (
                        <div className={styles.task} key={item.id} id={item.id}
                             onClick={e => openModalWindow(Number(e.currentTarget.id))}
                             style={doesShow ? (
                                 item.isComplete ? {opacity: 0.3} : null
                             ) : (
                                 item.isComplete ? {display: 'none'} : null
                             )}>

                            <Img className={styles.smallImg} src=
                                {'http://cdn1.savepice.ru/uploads/2017/8/1/3c1552e58ac7137efcc222e24ee08d83-full.png'}/>

                            <p className={styles.smallDescription}>
                                {item.title}<br/>
                                <i>{item.description}</i>
                            </p>

                            <span className={styles.strip} style={{backgroundColor: color}}/>

                            {page === '/my_tasks' ? (
                                <div className={styles.actions}>
                                    {!item.isComplete ? (
                                        <Img className={styles.actionsImg}
                                             src={'http://www.freeiconspng.com/uploads/edit-pen-write-icon--2.png'}
                                             onClick={function(e) {
                                                 e.stopPropagation();
                                                 setIdOfEditTask(e.target.parentNode.parentNode.id);
                                             }}/>
                                    ) : (
                                        <div/>
                                    )}
                                    <Img className={styles.actionsImg}
                                         src={'https://cdn2.iconfinder.com/data/icons/flat-ui-icons-24-px/24/cross-24-512.png'}
                                         onClick={function (e) {
                                             e.stopPropagation();
                                             deleteTheTask( Number(e.target.parentNode.parentNode.id) );
                                         }}/>
                                </div>
                            ) : (
                                <div/>
                            )}
                        </div>
                    )}, this)}
            </div>
        ) : (
            <Col smOffset={4}><div className={styles.tasksLoading}>Подождите, задачи загружаются...</div></Col>
        );

        return (
            tasks
        )
    }
}

