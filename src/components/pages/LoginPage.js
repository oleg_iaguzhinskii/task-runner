import React, { Component } from 'react'
import LoginForm from '../LoginForm'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { login, handleLoginText } from '../../actions/LoginFormActions'
import { setOpenedPage } from '../../actions/NavActions'


class LoginPage extends Component {
    render() {
        const { errorMessage, isLogged, isLoginTextInvalid } = this.props.data;
        const { login, handleLoginText, setOpenedPage } = this.props;
        return (
            <LoginForm login={login} errorMessage={errorMessage} isLogged={isLogged} setOpenedPage={setOpenedPage}
                       handleLoginText={handleLoginText} isLoginTextInvalid={isLoginTextInvalid} />
        )
    }
}

function mapStateToProps(state) {
    return {
        data: state.loginForm
    }
}

function mapDispatchToProps(dispatch) {
    return {
        login: bindActionCreators(login, dispatch),
        handleLoginText: bindActionCreators(handleLoginText, dispatch),
        setOpenedPage: bindActionCreators(setOpenedPage, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)

