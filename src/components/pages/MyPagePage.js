import React, { Component } from 'react'
import { connect } from 'react-redux'
import styles from '../../../styles/components/myPage.css'
import { Table, Row, Col } from 'react-bootstrap'

class MyPagePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isReady: false,
            numberOfNotCompletedTasks: 0,
            numberOfCompletedTasks: 0
        };
    }

    componentWillMount() {
        const self = this;
        const { loggedUserLogin } = this.props.data;
        let { numberOfCompletedTasks, numberOfNotCompletedTasks } = this.state;

        setTimeout(function() {

            fetch('http://localhost:4000/users')
                .then(function (response) {
                    return response.json();
                })
                .then(function (users) {
                    const user = users.filter(u => u.login === loggedUserLogin)[0];

                    const { name, surname, birthday, city, hobbies } = user;

                    fetch('http://localhost:4000/tasks')
                        .then(function (response) {
                            return response.json();
                        })
                        .then(function (tasks) {
                            const tasksList = tasks.filter(t => t.login === loggedUserLogin);

                            tasksList.map(function(t) {

                                if (t.isComplete) {
                                    numberOfCompletedTasks++;
                                } else {
                                    numberOfNotCompletedTasks++;
                                }

                            });

                            self.setState({
                                isReady: true,
                                numberOfNotCompletedTasks: numberOfNotCompletedTasks,
                                numberOfCompletedTasks: numberOfCompletedTasks,
                                name: name,
                                surname: surname,
                                birthday: birthday,
                                city: city,
                                hobbies: hobbies
                            });
                        })
                        .catch(function (ex) {
                            console.log('parsing failed: ' + ex);
                        });
                })
                .catch(function (ex) {
                    console.log('parsing failed: ' + ex);
                });
        }, 1000);

    }

    render() {
        const { isReady, numberOfNotCompletedTasks, numberOfCompletedTasks, name, surname, birthday, city, hobbies } = this.state;

        const myPage = isReady ? (
            <div>
                <Table striped bordered condensed hover>
                    <thead>
                        <tr><Row className="show-grid">
                            <Col smOffset={1} sm={11}><th>Статистика по задачам:</th></Col>
                        </Row></tr>
                    </thead>
                    <tbody>
                        <tr><Row className="show-grid">
                            <Col smOffset={2} sm={7}><td>выполненных</td></Col>
                            <Col sm={3}><td>{numberOfCompletedTasks}</td></Col>
                        </Row></tr>
                        <tr><Row className="show-grid">
                            <Col smOffset={2} sm={7}><td>в процессе выполнения</td></Col>
                            <Col sm={3}><td>{numberOfNotCompletedTasks}</td></Col>
                        </Row></tr>
                    </tbody>
                </Table>
                <Table striped bordered condensed hover>
                    <thead>
                        <tr><Row className="show-grid">
                            <Col smOffset={1} sm={11}><th>Ваши данные:</th></Col>
                        </Row></tr>
                    </thead>
                    <tbody>
                        <tr><Row className="show-grid">
                            <Col smOffset={2} sm={4}><td>имя</td></Col>
                            <Col sm={6}><td>{name}</td></Col>
                        </Row></tr>
                        <tr><Row className="show-grid">
                            <Col smOffset={2} sm={4}><td>фамилия</td></Col>
                            <Col sm={6}><td>{surname}</td></Col>
                        </Row></tr>
                        <tr><Row className="show-grid">
                            <Col smOffset={2} sm={4}><td>дата рождения</td></Col>
                            <Col sm={6}><td>{birthday}</td></Col>
                        </Row></tr>
                        <tr><Row className="show-grid">
                            <Col smOffset={2} sm={4}><td>город</td></Col>
                            <Col sm={6}><td>{city}</td></Col>
                        </Row></tr>
                        <tr><Row className="show-grid">
                            <Col smOffset={2} sm={4}><td>интересы</td></Col>
                            <Col sm={6}><td>{hobbies}</td></Col>
                        </Row></tr>
                    </tbody>
                </Table>
            </div>
        ) : <div className={styles.dataLoading}>Обновление...</div>;

        return <div>{myPage}</div>
    }

}

function mapStateToProps(state) {
    return {
        data: state.loginForm
    }
}

export default connect(mapStateToProps)(MyPagePage);
