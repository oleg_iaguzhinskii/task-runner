import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { openMyModalWindow, closeMyModalWindow, setIdOfEditTask } from '../../actions/TasksActions'
import { resetForm } from '../../actions/EditTaskActions'
import { sort } from '../../actions/SortingActions'
import Sorting from '../Sorting'
import Tasks from '../Tasks'
import ModalWindow from '../ModalWindow'
import EditTask from '../EditTask'

class MyTasksPage extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentWillMount() {
        const self = this;
        const { loggedUserLogin } = this.props.loginForm;

        setTimeout(function() {
            fetch('http://localhost:4000/tasks')
                .then(function (response) {
                    return response.json();
                })
                .then(function (tasks) {
                    const tasksList = tasks.filter(t => t.login === loggedUserLogin);

                    self.setState({
                        myTasks: tasksList
                    });
                })
                .catch(function (ex) {
                    console.log('parsing failed: ' + ex);
                });
        }, 1000);
    }

    completeTheTask(id) {
        const self = this;
        const task = this.state.myTasks.filter( t => t.id === id)[0];
        const {closeMyModalWindow} = this.props;

        fetch('http://localhost:4000/tasks/' + task.id, {
            method: 'put',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "login": task.login,
                "isPrivate": task.isPrivate,
                "title": task.title,
                "date": task.date,
                "dateTill": task.dateTill,
                "remindMeIn": task.remindMeIn,
                "category": task.category,
                "subcategory": task.subcategory,
                "description": task.description,
                "tag": task.tag,
                "isComplete": true,
                "id": task.id
            })
        })
            .then(function(response) {

                // Костыль v.2.0
                if ( !task.isComplete ) {
                    let { myTasks } = self.state;
                    const ind = myTasks.indexOf(task);
                    task.isComplete = true;
                    myTasks.splice(ind, 1, task);

                    self.setState({
                        myTasks: myTasks
                    });
                }

                closeMyModalWindow(id);
                return response.json();
            })
            .catch(function(ex) {
                console.log('parsing failed: ' +ex);
            });
    }

    editTask(task) {
        const self = this;
        const { setFieldEmpty, setIdOfEditTask } = this.props;
        const { date, id } = task;
        let { textTitle, textTextArea, isPrivate, dateTill, hoursToRemind, category, subcategory, tag } =
            this.props.createTask;
        const { loggedUserLogin } = this.props.loginForm;

        textTextArea = autoTextEdit(textTextArea);
        textTitle = autoTextEdit(textTitle);

        if (textTextArea === undefined || textTitle === undefined)
            return;

        const obj = {
            "login": loggedUserLogin,
            "isPrivate": isPrivate,
            "title": textTitle,
            "date": date,
            "dateTill": dateTill,
            "remindMeIn": hoursToRemind,
            "category": category,
            "subcategory": subcategory,
            "description": textTextArea,
            "tag": tag,
            "isComplete": false,
            "id": id
        };

        fetch('http://localhost:4000/tasks/' + id, {
            method: 'put',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(obj)
        })
            .then(function(response) {
                // Костыль v.2.1
                let { myTasks } = self.state;
                const ind = myTasks.indexOf(task);
                myTasks.splice(ind, 1, obj);

                self.setState({
                    myTasks: myTasks
                });

                setIdOfEditTask(null);
                ::self.reset();

                return response.json()
            })
            .catch(function(ex) {
                console.log('parsing failed: ', ex)
            });

        function autoTextEdit(text) {
            let sentences = [];
            text = text.replace(/\[.+\]/, '');

            if (text.match(/\S/) === null) {
                setFieldEmpty();
                return;
            }

            text = text.replace(/^\s+/, '').replace(/\s+$/, '')
                .replace(/\s{2,}/g, ' ');

            sentences = text.toLowerCase().match(/[^.]+\.?( *|$)/g);
            sentences = sentences.map(e => e.charAt(0).toUpperCase() + e.slice(1));

            text = sentences.join('');

            return text;
        }
    }

    reset() {
        const { resetForm, setIdOfEditTask } = this.props;
        setIdOfEditTask(null);
        resetForm();
    }

    deleteTheTask(id) {
        const self = this;
        const task = this.state.myTasks.filter( t => t.id === id)[0];

        fetch('http://localhost:4000/tasks/' + id, {
            method: 'delete',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(function(response) {

                // Костыль v.2.1
                let { myTasks } = self.state;
                const ind = myTasks.indexOf(task);
                myTasks.splice(ind, 1);

                self.setState({
                    myTasks: myTasks
                });

                return response.json();
            })
            .catch(function(ex) {
                console.log('parsing failed: ' +ex);
            });
    }

    render() {
        const { openMyModalWindow, closeMyModalWindow, sort, setIdOfEditTask } = this.props;
        const { myTasks } = this.state;
        const { openedMyModalWindow, id } = this.props.data;
        const { sortBy, descending } = this.props.sortData;
        const { page } = this.props.nav;
        const { doesShow } = this.props.myPage;

        if (myTasks) {
            myTasks.sort((a, b) => descending
                ? a[sortBy] < b[sortBy]
                : a[sortBy] > b[sortBy]
            );
        }

        const editTask = id && myTasks ? (
            <EditTask task={myTasks.filter(t => t.id === Number(id) )[0]} setIdOfEditTask={setIdOfEditTask}
                      editTask={::this.editTask} reset={::this.reset}/>
        ) : null;

        return (
                <div>
                    <Sorting sort={sort} sortBy={sortBy} descending={descending}/>
                    <Tasks tasksList={myTasks} page={page} openModalWindow={openMyModalWindow} doesShow={doesShow}
                           setIdOfEditTask={setIdOfEditTask} deleteTheTask={::this.deleteTheTask}/>
                    <ModalWindow tasksList={myTasks} page={page} openedModalWindow={openedMyModalWindow}
                                 closeModalWindow={closeMyModalWindow} completeTheTask={::this.completeTheTask}/>
                    {editTask}
                </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        data: state.tasks,
        createTask: state.createTask,
        loginForm: state.loginForm,
        nav: state.nav,
        sortData: state.sorting,
        myPage: state.myPage
    }
}

function mapDispatchToProps(dispatch) {
    return {
        openMyModalWindow: bindActionCreators(openMyModalWindow, dispatch),
        closeMyModalWindow: bindActionCreators(closeMyModalWindow, dispatch),
        sort: bindActionCreators(sort, dispatch),
        setIdOfEditTask: bindActionCreators(setIdOfEditTask, dispatch),
        resetForm: bindActionCreators(resetForm, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyTasksPage)