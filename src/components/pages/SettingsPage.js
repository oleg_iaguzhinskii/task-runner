import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { handleShowing } from '../../actions/MyPageActions'
import { Form, Checkbox, FormGroup, Col, ControlLabel } from 'react-bootstrap'

class MyPagePage extends Component {
    render() {
        const { doesShow } = this.props.myPage;
        const { handleShowing } = this.props;

        return (
            <Form horizontal style={{fontSize: '20px'}}>
                <FormGroup>
                    <Col componentClass={ControlLabel} sm={7}>Отображать выполненные задачи?</Col>
                    <Col sm={3}>
                        {doesShow ? (
                            <Checkbox onChange={ e => handleShowing(e.target.checked)} checked>&nbsp;Да</Checkbox>
                        ) : (
                            <Checkbox onChange={ e => handleShowing(e.target.checked)}>&nbsp;Нет</Checkbox>
                        )}
                    </Col>
                </FormGroup>
            </Form>
        )
    }

}

function mapStateToProps(state) {
    return {
        myPage: state.myPage
    }
}

function mapDispatchToProps(dispatch) {
    return {
        handleShowing: bindActionCreators(handleShowing, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyPagePage)
