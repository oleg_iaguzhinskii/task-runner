import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import styles from '../../../styles/components/createTasks.css'
import { Form, FormGroup, Col, ControlLabel, FormControl, Checkbox, Button } from 'react-bootstrap'
import {
    setTextTitle,
    setTextTextArea,
    handlePrivacy,
    onChangeField,
    setFieldEmpty,
    selectCategory,
    selectSubcategory,
    selectDateTill,
    setHoursToRemind,
    setTag,
    resetForm
} from '../../actions/CreateTaskActions'

class CreateTaskPage extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        //Получения списка категорий и подкатегорий
        const self = this;
        const { selectCategory } = this.props;

        fetch('http://localhost:4000/categories')
            .then(function(response) {
                return response.json();
            })
            .then(function(category) {
                self.setState({
                    categoriesList: category,
                    subcategoriesList: category[0].subcategories
                });
                selectCategory(category[0].title, category[0].subcategories[0].title);
            })
            .catch(function(ex) {
                console.log('parsing failed: ' + ex);
            });

        //Получения сегодняшней даты
        const today = new Date();
        let yy = today.getFullYear();
        let mm = today.getMonth() + 1;
        let dd = today.getDate();

        if (yy < 10) yy = '0' + yy;
        if (mm < 10) mm = '0' + mm;
        if (dd < 10) dd = '0' + dd;

        this.setState({
            today: '' + yy + '-' + mm + '-' + dd
        });
    }

    handleTextTitle(text) {
        const { onChangeField, setTextTitle } = this.props;
        setTextTitle(text);
        onChangeField();
    }

    handleCategorySelection(category) {
        const { categoriesList } = this.state;
        const { selectCategory } = this.props;
        category = categoriesList.filter(c => c.title === category)[0];
        const param2 = category.subcategories[0] ? category.subcategories[0].title : undefined;

        this.setState({
            subcategoriesList: category.subcategories
        });
        selectCategory(category.title, param2);
    }

    handleTextTextArea(text) {
        const { onChangeField, setTag, setTextTextArea } = this.props;
        const tag = checkTag(text);

        setTextTextArea(text);
        onChangeField();
        setTag(tag);

        function checkTag(text) {
            text = text.toLowerCase().match(/\[.*\]/);

            if(text) {
                switch( text[0].slice(1, -1).replace(/ё/, 'е') ) {
                    case 'красный':
                        text = 1;
                        break;
                    case 'зеленый':
                        text = 2;
                        break;
                    case 'желтый':
                        text = 3;
                        break;
                    default:
                        text = 0;
                }
            } else {
                text = 4;
            }

            return text;
        }
    }

    createTask() {
        const self = this;
        const { setFieldEmpty } = this.props;
        let { textTitle, textTextArea, isPrivate, dateTill, hoursToRemind, category, subcategory, tag } = this.props.createTask;
        const { loggedUserLogin } = this.props.loginForm;

        textTextArea = autoTextEdit(textTextArea);
        textTitle = autoTextEdit(textTitle);

        if (textTextArea === undefined || textTitle === undefined)
            return;

        const obj = {
            "login": loggedUserLogin,
            "isPrivate": isPrivate,
            "title": textTitle,
            "date": new Date(),
            "dateTill": dateTill || 'Бессрочно',
            "remindMeIn": hoursToRemind,
            "category": category,
            "subcategory": subcategory,
            "description": textTextArea,
            "tag": tag,
            "isComplete": false
        };

        fetch('http://localhost:4000/tasks/', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(obj)
        })
            .then(function(response) {
                ::self.reset();

                return response.json()
            })
            .catch(function(ex) {
                console.log('parsing failed: ', ex)
            });

        function autoTextEdit(text) {
            let sentences = [];
            text = text.replace(/\[.+\]/, '');

            if (text.match(/\S/) === null) {
                setFieldEmpty();
                return;
            }

            text = text.replace(/^\s+/, '').replace(/\s+$/, '')
                .replace(/\s{2,}/g, ' ');

            sentences = text.toLowerCase().match(/[^.]+\.?( *|$)/g);
            sentences = sentences.map(e => e.charAt(0).toUpperCase() + e.slice(1));

            text = sentences.join('');

            return text;
        }
    }

    reset() {
        const { resetForm } = this.props;
        resetForm();
    }

    render() {
        const { categoriesList, subcategoriesList, today } = this.state;
        const { handlePrivacy, selectSubcategory, selectDateTill, setHoursToRemind } = this.props;
        const { textTitle, textTextArea, dateTill, isPrivate, hoursToRemind, typeWordOfHours,
            errorMessage } = this.props.createTask;

        const optionOfCategory = categoriesList ? (
            categoriesList.map(function (item) {
                return (
                    <option key={item.id}>{item.title}</option>
                )
            })
        ) : (
            <option>Нет категорий</option>
        );

        const optionOfSubcategory = subcategoriesList ? (
            subcategoriesList.map(function (item) {
                return (
                    <option key={item.id}>{item.title}</option>
                )
            })
        ) : (
            <option>Нет подкатегорий</option>
        );

        let { isSubmitDisabled } = { isSubmitDisabled: false };
        if (errorMessage) {
            isSubmitDisabled = true;
        }

        return (
            <Form horizontal style={{fontSize: '20px'}}>
                <FormGroup controlId="formHorizontalText">
                    <Col componentClass={ControlLabel} sm={3}>Название</Col>
                    <Col sm={7}>
                        <FormControl type="text" onChange={(e) => ::this.handleTextTitle(e.target.value)}
                                     value={textTitle} placeholder="Дайте название Вашей задаче" />
                    </Col>
                </FormGroup>
                <FormGroup>
                    <Col componentClass={ControlLabel} sm={3}>Приватность</Col>
                    <Col sm={7}>
                        {isPrivate ? (
                            <Checkbox onChange={ e => handlePrivacy(e.target.checked)} checked>(в мои задачи)</Checkbox>
                        ) : (
                            <Checkbox onChange={ e => handlePrivacy(e.target.checked)}>(в общие задачи)</Checkbox>
                        )}
                    </Col>
                </FormGroup>
                <FormGroup controlId="formControlsSelect">
                    <Col componentClass={ControlLabel} sm={3}>Категория</Col>
                    <Col sm={7}>
                        <FormControl componentClass="select" onChange={ e => ::this.handleCategorySelection(e.target.value)}>
                            {optionOfCategory}
                        </FormControl>
                    </Col>
                </FormGroup>
                <FormGroup controlId="formControlsSelect">
                    <Col componentClass={ControlLabel} sm={3}>Подкатегория</Col>
                    <Col sm={7}>
                        <FormControl componentClass="select" onChange={ e => selectSubcategory(e.target.value)}>
                            {optionOfSubcategory}
                        </FormControl>
                    </Col>
                </FormGroup>
                <FormGroup>
                    <Col componentClass={ControlLabel} sm={3}>Дата завершения</Col>
                    <Col sm={7}>
                        <input type="date" id="dateTill" value={dateTill}
                               min={today} onChange={ e => selectDateTill(e.target.value)}/>
                    </Col>
                </FormGroup>
                <FormGroup>
                    <Col componentClass={ControlLabel} sm={3}>Напомнить за</Col>
                    <Col sm={7}>
                        <span>{hoursToRemind} {typeWordOfHours}</span>
                        <input type="range" id="remindMeIn" value={hoursToRemind} min="1" max="48"
                               onChange={ e => setHoursToRemind(e.target.value)}/>
                    </Col>
                </FormGroup>
                <FormGroup controlId="formControlsTextarea">
                    <Col componentClass={ControlLabel} sm={3}>Описание</Col>
                    <Col sm={7}>
                        <FormControl componentClass="textarea" placeholder="Опишите Вашу задачу" value={textTextArea}
                                     onChange={e => ::this.handleTextTextArea(e.target.value)} />
                    </Col>
                </FormGroup>
                <FormGroup>
                    <Col smOffset={3} sm={7}>
                        <FormControl.Static className={styles.warning}>{errorMessage}</FormControl.Static>
                    </Col>
                </FormGroup>
                <Col smOffset={3}>
                    {isSubmitDisabled ? (
                        <Button bsStyle="primary" type="submit" disabled>Создать</Button>
                    ): (
                        <Button bsStyle="primary" onClick={::this.createTask}>Создать</Button>
                    )}
                    &nbsp;<Button onClick={::this.reset}>Очистить</Button>
                </Col>
            </Form>
        )
    }
}

function mapStateToProps(state) {
    return {
        createTask: state.createTask,
        loginForm: state.loginForm
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setTextTitle: bindActionCreators(setTextTitle, dispatch),
        setTextTextArea: bindActionCreators(setTextTextArea, dispatch),
        handlePrivacy: bindActionCreators(handlePrivacy, dispatch),
        onChangeField: bindActionCreators(onChangeField, dispatch),
        setFieldEmpty: bindActionCreators(setFieldEmpty, dispatch),
        selectCategory: bindActionCreators(selectCategory, dispatch),
        selectSubcategory: bindActionCreators(selectSubcategory, dispatch),
        selectDateTill: bindActionCreators(selectDateTill, dispatch),
        setHoursToRemind: bindActionCreators(setHoursToRemind, dispatch),
        setTag: bindActionCreators(setTag, dispatch),
        resetForm: bindActionCreators(resetForm, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateTaskPage)