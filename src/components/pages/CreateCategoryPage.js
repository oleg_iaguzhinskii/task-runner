import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Form, FormGroup, Col, ControlLabel, FormControl, Button, Radio } from 'react-bootstrap'
import {
    setTypeOfCreation,
    setTextTitle,
    setTextTextArea,
    onChangeField,
    setFieldEmpty,
    selectCategory,
    selectSubcategory,
    resetForm
} from '../../actions/CreateCategoryActions'
import autoTextEdit from '../../utils/autoTextEdit'
import styles from '../../../styles/components/createTasks.css'

class CreateCategoryPage extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentWillMount() {
        //Получения списка категорий и подкатегорий
        const self = this;
        const { selectCategory } = this.props;

        fetch('http://localhost:4000/categories')
            .then(function(response) {
                return response.json();
            })
            .then(function(category) {
                self.setState({
                    categoriesList: category,
                    subcategoriesList: category[0].subcategories
                });
                selectCategory(category[0].title, category[0].subcategories[0].title);
            })
            .catch(function(ex) {
                console.log('parsing failed: ' + ex);
            });
    }

    handleTextTitle(text) {
        const { onChangeField, setTextTitle } = this.props;
        setTextTitle(text);
        onChangeField();
    }

    handleTextTextArea(text) {
        const { onChangeField, setTextTextArea } = this.props;
        setTextTextArea(text);
        onChangeField();
    }

    handleCategorySelection(category) {
        const { categoriesList } = this.state;
        const { selectCategory } = this.props;
        category = categoriesList.filter(c => c.title === category)[0];
        const param2 = category.subcategories[0] ? category.subcategories[0].title : undefined;

        this.setState({
            subcategoriesList: category.subcategories
        });

        selectCategory(category.title, param2);
    }

    createCategory() {
        const self = this;
        let { textTitle, textTextArea } = this.props.createCategory;

        textTextArea = autoTextEdit(textTextArea);
        textTitle = autoTextEdit(textTitle);

        if (textTextArea === undefined || textTitle === undefined)
            return;

        const obj = {
            "subcategories": [],
            "title": textTitle,
            "description": textTextArea
        };

        fetch('http://localhost:4000/categories/', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(obj)
        })
            .then(function(response) {
                // Костыль v.2.1
                let { categoriesList } = self.state;
                categoriesList.push(obj);

                self.setState({
                    categoriesList: categoriesList
                });

                ::self.reset();

                return response.json()
            })
            .catch(function(ex) {
                console.log('parsing failed: ', ex)
            });
    }

    createSubcategory() {
        const self = this;
        let { textTitle, textTextArea, category } = this.props.createCategory;
        const { categoriesList } = this.state;

        category = categoriesList.filter(c => c.title === category)[0]; //теперь это объект
        const { subcategories = [], title, description, id } = category;

        textTextArea = autoTextEdit(textTextArea);
        textTitle = autoTextEdit(textTitle);

        if (textTextArea === undefined || textTitle === undefined)
            return;

        const subcategoryObj = {
            "title": textTitle,
            "description": textTextArea,
            "id": subcategories.length + 1
        };
        subcategories.push(subcategoryObj);

        const obj = {
            "subcategories": subcategories,
            "title": title,
            "description": description,
            "id": id
        };

        fetch('http://localhost:4000/categories/' + id, {
            method: 'put',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(obj)
        })
            .then(function(response) {
                self.setState({
                    categoriesList: categoriesList
                });

                ::self.reset();

                return response.json()
            })
            .catch(function(ex) {
                console.log('parsing failed: ', ex)
            });
    }

    reset() {
        const { resetForm } = this.props;
        resetForm();
    }

    render() {
        const { categoriesList } = this.state;
        const { setTypeOfCreation } = this.props;
        const { typeOfCreation, textTitle, textTextArea, errorMessage } = this.props.createCategory;

        const optionOfCategory = categoriesList ? (
            categoriesList.map(function (item) {
                return (
                    <option key={item.id}>{item.title}</option>
                )
            })
        ) : (
            <option>Нет категорий</option>
        );

        let isSubmitDisabled = false ;
        if (errorMessage) {
            isSubmitDisabled = true;
        }

        return (
            <Form horizontal style={{fontSize: '20px'}}>
                <FormGroup>
                    <Col componentClass={ControlLabel} sm={3}>Создать:</Col>
                    <Col sm={7}>
                        <Radio name="radioGroup"  onClick={() => setTypeOfCreation('category')}
                               defaultChecked>категорию</Radio>
                        <Radio name="radioGroup"  onClick={() => setTypeOfCreation('subcategory')}>подкатегорию</Radio>
                    </Col>
                </FormGroup>
                {typeOfCreation === 'subcategory' && (
                    <FormGroup controlId="formControlsSelect">
                        <Col componentClass={ControlLabel} sm={3}>... в категории:</Col>
                        <Col sm={7}>
                            <FormControl componentClass="select" onChange={ e => ::this.handleCategorySelection(e.target.value)}>
                                {optionOfCategory}
                            </FormControl>
                        </Col>
                    </FormGroup>
                )}
                <FormGroup controlId="formHorizontalText">
                    <Col componentClass={ControlLabel} sm={3}>Название</Col>
                    <Col sm={7}>
                        <FormControl type="text" onChange={(e) => ::this.handleTextTitle(e.target.value)}
                                     value={textTitle} placeholder="Дайте название Вашей категории/подкатегории" />
                    </Col>
                </FormGroup>
                <FormGroup controlId="formControlsTextarea">
                    <Col componentClass={ControlLabel} sm={3}>Описание</Col>
                    <Col sm={7}>
                        <FormControl componentClass="textarea" placeholder="Опишите Вашу категорию/подкатегорию"
                                     value={textTextArea} onChange={e => ::this.handleTextTextArea(e.target.value)} />
                    </Col>
                </FormGroup>
                <FormGroup>
                    <Col smOffset={3} sm={7}>
                        <FormControl.Static className={styles.warning}>{errorMessage}</FormControl.Static>
                    </Col>
                </FormGroup>
                <Col smOffset={3}>
                    {isSubmitDisabled ? (
                        <Button bsStyle="primary" type="submit" disabled>Создать</Button>
                    ): (
                        <Button bsStyle="primary" onClick={typeOfCreation === 'category' ?
                            ::this.createCategory :
                            ::this.createSubcategory}>Создать</Button>
                    )}
                    &nbsp;<Button onClick={::this.reset}>Очистить</Button>
                </Col>
            </Form>
        )
    }
}

function mapStateToProps(state) {
    return {
        createCategory: state.createCategory
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setTypeOfCreation: bindActionCreators(setTypeOfCreation, dispatch),
        setTextTitle: bindActionCreators(setTextTitle, dispatch),
        setTextTextArea: bindActionCreators(setTextTextArea, dispatch),
        onChangeField: bindActionCreators(onChangeField, dispatch),
        setFieldEmpty: bindActionCreators(setFieldEmpty, dispatch),
        selectCategory: bindActionCreators(selectCategory, dispatch),
        selectSubcategory: bindActionCreators(selectSubcategory, dispatch),
        resetForm: bindActionCreators(resetForm, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateCategoryPage)

