import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import styles from '../../styles/components/createTasks.css'
import { Modal, Form, FormGroup, Col, ControlLabel, FormControl, Checkbox, Button } from 'react-bootstrap'

import {
    setTextTitle,
    setTextTextArea,
    handlePrivacy,
    onChangeField,
    setFieldEmpty,
    selectCategory,
    selectSubcategory,
    selectDateTill,
    setHoursToRemind,
    setTag,
    resetForm
} from '../actions/EditTaskActions'


class EditTask extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentWillMount() {
        //Получения списка категорий и подкатегорий
        const self = this;
        const { task, selectCategory, setTextTitle, setTextTextArea, handlePrivacy, selectSubcategory, selectDateTill,
            setHoursToRemind, setTag } = this.props;
        const { category } = task;
        let tagString = task.tag;

        switch( tagString ) {
            case 1:
                tagString = '[красный]';
                break;
            case 2:
                tagString = '[зеленый]';
                break;
            case 3:
                tagString = '[желтый]';
                break;
            default:
                tagString = '';
        }

        fetch('http://localhost:4000/categories')
            .then(function(response) {
                return response.json();
            })
            .then(function(categoriesList) {
                self.setState({
                    categoriesList: categoriesList,
                    subcategoriesList: categoriesList.filter(c => c.title === category)[0].subcategories
                });
            })
            .catch(function(ex) {
                console.log('parsing failed: ' + ex);
            });

        //Установка сегодняшней даты
        const today = new Date();
        let yy = today.getFullYear();
        let mm = today.getMonth() + 1;
        let dd = today.getDate();

        if (yy < 10) yy = '0' + yy;
        if (mm < 10) mm = '0' + mm;
        if (dd < 10) dd = '0' + dd;

        this.setState({
            today: '' + yy + '-' + mm + '-' + dd
        });

        //Кладём в store данные задачи
        const { title, description, isPrivate, subcategory, dateTill, remindMeIn, tag } = task;

        setTextTitle(title);
        setTextTextArea(tagString + description);
        handlePrivacy(isPrivate);
        selectCategory(category);
        selectSubcategory(subcategory);
        selectDateTill(dateTill);
        setHoursToRemind(remindMeIn);
        setTag(tag);
    }

    handleTextTitle(text) {
        const { onChangeField, setTextTitle } = this.props;
        setTextTitle(text);
        onChangeField();
    }

    handleCategorySelection(category) {
        const { categoriesList } = this.state;
        const { selectCategory } = this.props;
        category = categoriesList.filter(c => c.title === category)[0];
        this.setState({
            subcategoriesList: category.subcategories
        });
        selectCategory(category.title, category.subcategories[0].title);
    }

    handleTextTextArea(text) {
        const { onChangeField, setTag, setTextTextArea } = this.props;
        const tag = checkTag(text);

        setTextTextArea(text);
        onChangeField();
        setTag(tag);

        function checkTag(text) {
            text = text.toLowerCase().match(/\[.*\]/);

            if(text) {
                switch( text[0].slice(1, -1).replace(/ё/, 'е') ) {
                    case 'красный':
                        text = 1;
                        break;
                    case 'зеленый':
                        text = 2;
                        break;
                    case 'желтый':
                        text = 3;
                        break;
                    default:
                        text = 0;
                }
            } else {
                text = 4;
            }

            return text;
        }
    }

    render() {
        const { categoriesList, subcategoriesList, today } = this.state;
        const { task, handlePrivacy, selectSubcategory, selectDateTill, setHoursToRemind, editTask, reset } = this.props;
        const { textTitle, textTextArea, category, subcategory, isPrivate, hoursToRemind, typeWordOfHours,
            errorMessage } = this.props.createTask;
        let { dateTill } = this.props.createTask;

        let isSubmitDisabled = false ;
        if (errorMessage) {
            isSubmitDisabled = true;
        }

        const optionOfCategory = categoriesList ? (
            categoriesList.map(function (item) {
                return (
                    <option key={item.id}>{item.title}</option>
                )
            })
        ) : (
            <option>Нет категорий</option>
        );

        const optionOfSubcategory = subcategoriesList ? (
            subcategoriesList.map(function (item) {
                return (
                    <option key={item.id}>{item.title}</option>
                )
            })
        ) : (
            <option>Нет подкатегорий</option>
        );

        const windowOfEditTask = task ? (
            <Modal.Dialog>
                <Modal.Header>
                    <Modal.Title>Редактирование задачи</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form horizontal style={{fontSize: '20px'}}>
                        <FormGroup controlId="formHorizontalText">
                            <Col componentClass={ControlLabel} sm={3}>Название</Col>
                            <Col sm={7}>
                                <FormControl type="text" onChange={(e) => ::this.handleTextTitle(e.target.value)}
                                             value={textTitle} placeholder="Дайте название Вашей задаче" />
                            </Col>
                        </FormGroup>
                        <FormGroup>
                            <Col componentClass={ControlLabel} sm={3}>Приватность</Col>
                            <Col sm={7}>
                                {isPrivate ? (
                                    <Checkbox onChange={ e => handlePrivacy(e.target.checked)} checked>(в мои задачи)</Checkbox>
                                ) : (
                                    <Checkbox onChange={ e => handlePrivacy(e.target.checked)}>(в общие задачи)</Checkbox>
                                )}
                            </Col>
                        </FormGroup>
                        <FormGroup controlId="formControlsSelect">
                            <Col componentClass={ControlLabel} sm={3}>Категория</Col>
                            <Col sm={7}>
                                <FormControl componentClass="select" value={category}
                                             onChange={ e => ::this.handleCategorySelection(e.target.value)}>
                                    {optionOfCategory}
                                </FormControl>
                            </Col>
                        </FormGroup>
                        <FormGroup controlId="formControlsSelect">
                            <Col componentClass={ControlLabel} sm={3}>Подкатегория</Col>
                            <Col sm={7}>
                                <FormControl componentClass="select" value={subcategory}
                                             onChange={ e => selectSubcategory(e.target.value)}>
                                    {optionOfSubcategory}
                                </FormControl>
                            </Col>
                        </FormGroup>
                        <FormGroup>
                            <Col componentClass={ControlLabel} sm={3}>Дата завершения</Col>
                            <Col sm={7}>
                                <input type="date" id="dateTill" value={dateTill}
                                       min={today} onChange={ e => selectDateTill(e.target.value)}/>
                            </Col>
                        </FormGroup>
                        <FormGroup>
                            <Col componentClass={ControlLabel} sm={3}>Напомнить за</Col>
                            <Col sm={7}>
                                <span>{hoursToRemind} {typeWordOfHours}</span>
                                <input type="range" id="remindMeIn" value={hoursToRemind} min="1" max="48"
                                       onChange={ e => setHoursToRemind(e.target.value)}/>
                            </Col>
                        </FormGroup>
                        <FormGroup controlId="formControlsTextarea">
                            <Col componentClass={ControlLabel} sm={3}>Описание</Col>
                            <Col sm={7}>
                                <FormControl componentClass="textarea" placeholder="Опишите Вашу задачу" value={textTextArea}
                                             onChange={e => ::this.handleTextTextArea(e.target.value)} />
                            </Col>
                        </FormGroup>
                        <FormGroup>
                            <Col smOffset={3} sm={7}>
                                <FormControl.Static className={styles.warning}>{errorMessage}</FormControl.Static>
                            </Col>
                        </FormGroup>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    {isSubmitDisabled ? (
                        <Button bsStyle="primary" type="submit" disabled>Сохранить</Button>
                    ): (
                        <Button bsStyle="primary" onClick={() => editTask(task)}>Сохранить</Button>
                    )}
                    <Button onClick={reset}>Закрыть</Button>&nbsp;
                </Modal.Footer>
            </Modal.Dialog>
        ) : null;

        return (
            windowOfEditTask
        )
    }
}

function mapStateToProps(state) {
    return {
        createTask: state.createTask,
        loginForm: state.loginForm,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setTextTitle: bindActionCreators(setTextTitle, dispatch),
        setTextTextArea: bindActionCreators(setTextTextArea, dispatch),
        handlePrivacy: bindActionCreators(handlePrivacy, dispatch),
        onChangeField: bindActionCreators(onChangeField, dispatch),
        setFieldEmpty: bindActionCreators(setFieldEmpty, dispatch),
        selectCategory: bindActionCreators(selectCategory, dispatch),
        selectSubcategory: bindActionCreators(selectSubcategory, dispatch),
        selectDateTill: bindActionCreators(selectDateTill, dispatch),
        setHoursToRemind: bindActionCreators(setHoursToRemind, dispatch),
        setTag: bindActionCreators(setTag, dispatch),
        resetForm: bindActionCreators(resetForm, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditTask)