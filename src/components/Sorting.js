import React, {Component} from 'react';
import { ButtonToolbar, Button, Col } from 'react-bootstrap'

export default class Sorting extends Component {
    render() {
        const { sort, sortBy, descending } = this.props;
        const sorting = [
            {
                name: 'date',
                val: 'По дате',
                id: 1
            },
            {
                name: 'tag',
                val: 'По приоритету',
                id: 2
            },
            {
                name: 'isComplete',
                val: 'По выполнению',
                id: 3
            },
            {
                name: 'category',
                val: 'По категории',
                id: 4
            }
        ];

        return (
            <Col mdOffset={3}>
                <ButtonToolbar>
                    {sorting.map(function(item) {
                        let text = item.val;
                        if (sortBy === item.name) {
                            text += descending ? ' \u2191' : ' \u2193'
                        }
                        return (
                            <Button key={item.id} id={item.id} bsStyle="primary" bsSize="large" style={{width: '9.5em'}}
                                    onClick={e => sort( Number(e.target.id), sortBy, descending)}>
                                {text}
                            </Button>
                        )
                    }, this)}
                </ButtonToolbar>
            </Col>
        )
    }
}
