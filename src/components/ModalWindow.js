import React, { Component } from 'react'
import styles from '../../styles/components/modalWindow.css'
import getDate from '../utils/getDate'
import { Modal, Button } from 'react-bootstrap'

export default class ModalWindow extends Component {
    render() {
        const { tasksList, openedModalWindow, closeModalWindow, completeTheTask, page } = this.props;
        const { id, title, category, subcategory, date, dateTill, description, isComplete } =
            tasksList && openedModalWindow ? tasksList.filter(t => t.id === openedModalWindow)[0]
            : { id: 0, title: '', category: '', subcategory: '',
                date: null, dateTill: undefined, description: '', isComplete: false };

        const modalWindow = tasksList && openedModalWindow ? (
            <div key={id} id={id} className="static-modal">
                <Modal.Dialog>
                    <Modal.Header>
                        <Modal.Title>{title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className={styles.groupAndDate}>
                            <div className={styles.inline}>
                                <p className={styles.category}>{category}</p>
                                <p className={styles.subcategory}>{subcategory}</p>
                            </div>
                            <div className={styles.inline}>
                                <p>До:&nbsp;
                                    <span className={styles.end}>
                                    { (dateTill === 'Бессрочно' || (dateTill && dateTill.length === 10) )
                                        ? dateTill
                                        : getDate( new Date(dateTill) )
                                    }
                                </span>
                                </p>
                                <p className={styles.start}>От:&nbsp;
                                    {getDate( new Date(date) )}</p>
                            </div>
                        </div>
                        <div className={styles.content}>
                            {description}
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        {page === '/my_tasks' ? (
                            isComplete ? (
                                <Button bsStyle="success" disabled>Выполнена</Button>
                            ) : (
                                <Button bsStyle="success"
                                        onClick=
                                            {e => completeTheTask(
                                                Number(e.target.parentNode.parentNode.parentNode.parentNode.parentNode.id)
                                            )}
                                >Завершить</Button>
                            )
                        ) : null}
                        <Button onClick={closeModalWindow}>Закрыть</Button>
                    </Modal.Footer>
                </Modal.Dialog>
            </div>
        ) : (
            null
        );

        return (
            modalWindow
        )
    }
}


