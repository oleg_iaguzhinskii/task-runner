import { SORT_TASK } from '../constants/sorting'

export function sort(column, sortBy, descending) {
    switch (column) {
        case 1:
            column = 'date';
            break;
        case 2:
            column = 'tag';
            break;
        case 3:
            column = 'isComplete';
            break;
        case 4:
            column = 'category';
            break;
        default:
            column = 'tag';
    }

    descending = sortBy === column && !descending;


    return function(dispatch) {
        dispatch({
            type: SORT_TASK,
            payload: {
                sortBy: column,
                descending: descending
            }
        })
    }
}
