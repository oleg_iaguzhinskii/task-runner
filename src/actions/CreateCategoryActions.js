import {
    EMPTY_FIELD,
    NOT_EMPTY_FIELD,
    SET_TEXT_TITLE,
    SET_TEXT_TEXT_AREA,
    SET_CATEGORY,
    SET_SUBCATEGORY,
    SET_TYPE_OF_CREATION,
    RESET_FORM
} from '../constants/createCategory'

export function setFieldEmpty() {
    return function(dispatch) {
        dispatch({
            type: EMPTY_FIELD,
            payload: {
                errorMessage: 'Поля не могут быть пустыми!'
            }
        })
    }
}

export function onChangeField() {
    return function(dispatch) {
        dispatch({
            type: NOT_EMPTY_FIELD
        })
    }
}

export function setTextTitle(text) {
    return function(dispatch) {
        dispatch({
            type: SET_TEXT_TITLE,
            payload: {
                textTitle: text
            }
        })
    }
}

export function setTextTextArea(text) {
    return function(dispatch) {
        dispatch({
            type: SET_TEXT_TEXT_AREA,
            payload: {
                textTextArea: text
            }
        })
    }
}

export function selectCategory(category, subcategory) {
    return function(dispatch) {
        dispatch({
            type: SET_CATEGORY,
            payload: {
                category: category
            }
        });
        dispatch({
            type: SET_SUBCATEGORY,
            payload: {
                subcategory: subcategory
            }
        })
    }
}

export function selectSubcategory(subcategory) {
    return function(dispatch) {
        dispatch({
            type: SET_SUBCATEGORY,
            payload: {
                subcategory: subcategory
            }
        })
    }
}

export function setTypeOfCreation(type) {
    if (type === 'category') {
        return function(dispatch) {
            dispatch({
                type: SET_TYPE_OF_CREATION,
                payload: {
                    typeOfCreation: 'category'
                }
            })
        }
    } else if (type === 'subcategory') {
        return function(dispatch) {
            dispatch({
                type: SET_TYPE_OF_CREATION,
                payload: {
                    typeOfCreation: 'subcategory'
                }
            })
        }
    }

}

export function resetForm() {
    return function(dispatch) {
        dispatch({
            type: RESET_FORM
        })
    }
}

