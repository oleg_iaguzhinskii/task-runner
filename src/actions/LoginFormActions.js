import { LOGIN_PENDING, LOGIN_SUCCESS, LOGIN_ERROR, INVALID_LOGIN_TEXT, VALID_LOGIN_TEXT } from '../constants/loginForm'

function loginPending() {
    return {
        type: LOGIN_PENDING
    }
}

function loginSuccess(json) {
    return {
        type: LOGIN_SUCCESS,
        payload: {
            loggedUserLogin: json.login
        }
    }
}

function loginError(text) {
    return {
        type: LOGIN_ERROR,
        payload: {
            errorMessage: text
        }
    }
}

export function login(login, password) {

    if (login && password) {
        login = login.toLowerCase();
        password = password.toLowerCase();
    }

    return function(dispatch) {

        dispatch( loginPending() );

        return fetch('http://localhost:4000/users')
            .then(function (response) {
                return response.json();
            })
            .then(function (users) {
                let user = users.filter(u => u.login === login)[0];

                if (user && user.password === password) {
                    dispatch( loginSuccess(user) );
                } else {
                    dispatch( loginError('Неправильно введён логин или пароль') );
                }

            })
            .catch(function (ex) {
                console.log('parsing failed: ' + ex);
            });
    }
}

export function handleLoginText(text) {
    text = text.toLowerCase();

    if (text.search( /[^0-9a-z]/i ) !== -1) {
        return function(dispatch) {
            dispatch({
                type: INVALID_LOGIN_TEXT,
                payload: {
                    errorMessage: 'Поле логин должно состоять только из чисел или латинских символов'
                }
            })
        };
    } else {
        return function(dispatch) {
            dispatch({
                type: VALID_LOGIN_TEXT,
                payload: {
                    errorMessage: null
                }
            })
        };
    }

}