import { LOG_OUT } from '../constants/loginForm'
import { SET_OPENED_PAGE } from '../constants/nav'

export function setOpenedPage(page) {
    return function(dispatch) {
        dispatch({
            type: SET_OPENED_PAGE,
            payload: {
                page: page
            }
        })
    }
}

export function logout() {
    return function(dispatch) {
        dispatch({
            type: LOG_OUT
        })
    }
}
