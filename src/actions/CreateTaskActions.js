import {
    EMPTY_FIELD,
    NOT_EMPTY_FIELD,
    SET_TEXT_TITLE,
    SET_TEXT_TEXT_AREA,
    SET_PRIVATE,
    DELETE_PRIVATE,
    SET_CATEGORY,
    SET_SUBCATEGORY,
    SET_DATE_TILL,
    SET_HOURS_TO_REMIND,
    SET_TAG,
    RESET_FORM
} from '../constants/createTask'

export function setFieldEmpty() {
    return function(dispatch) {
        dispatch({
            type: EMPTY_FIELD,
            payload: {
                errorMessage: 'Поля не могут быть пустыми!'
            }
        })
    }
}

export function onChangeField() {
    return function(dispatch) {
        dispatch({
            type: NOT_EMPTY_FIELD
        })
    }
}

export function setTextTitle(text) {
    return function(dispatch) {
        dispatch({
            type: SET_TEXT_TITLE,
            payload: {
                textTitle: text
            }
        })
    }
}

export function setTextTextArea(text) {
    return function(dispatch) {
        dispatch({
            type: SET_TEXT_TEXT_AREA,
            payload: {
                textTextArea: text
            }
        })
    }
}

export function handlePrivacy(checked) {
    return function(dispatch) {

        if (!checked) {
            dispatch({
                type: DELETE_PRIVATE
            });
        } else {
            dispatch({
                type: SET_PRIVATE
            });
        }

    }
}

export function selectCategory(category, subcategory) {
    return function(dispatch) {
        dispatch({
            type: SET_CATEGORY,
            payload: {
                category: category
            }
        });
        dispatch({
            type: SET_SUBCATEGORY,
            payload: {
                subcategory: subcategory
            }
        })
    }
}

export function selectSubcategory(subcategory) {
    return function(dispatch) {
        dispatch({
            type: SET_SUBCATEGORY,
            payload: {
                subcategory: subcategory
            }
        })
    }
}

export function selectDateTill(date) {

    if ( !(date && date.length > 0)) {
        date = 'Бессрочно';
    }

    return function(dispatch) {
        dispatch({
            type: SET_DATE_TILL,
            payload: {
                dateTill: date
            }
        })
    }
}

export function setHoursToRemind(value) {
    let text = '';
    switch ( Number(value) ) {
        case 1:
        case 21:
        case 31:
        case 41:
            text = 'час';
            break;
        case 2:
        case 3:
        case 4:
        case 22:
        case 23:
        case 24:
        case 32:
        case 33:
        case 34:
        case 42:
        case 43:
        case 44:
            text = 'часа';
            break;
        default:
            text = 'часов';
            break;
    }

    return function(dispatch) {
        dispatch({
            type: SET_HOURS_TO_REMIND,
            payload: {
                hoursToRemind: value,
                typeWordOfHours: text
            }
        })
    }
//
}

export function setTag(tag) {
    return function(dispatch) {
        dispatch({
            type: SET_TAG,
            payload: {
                tag: tag,
                errorMessage: tag === 0 ? 'Неправильный тег!' : null
            }
        })
    }
}

export function resetForm() {
    return function(dispatch) {
        dispatch({
            type: RESET_FORM
        })
    }
}