import {
    OPEN_COMMON_MODAL_WINDOW,
    OPEN_MY_MODAL_WINDOW,
    CLOSE_COMMON_MODAL_WINDOW,
    CLOSE_MY_MODAL_WINDOW,
    SET_ID_OF_EDIT_TASK
} from '../constants/tasks'

export function openCommonModalWindow(id) {
    return function(dispatch) {
        dispatch({
            type: OPEN_COMMON_MODAL_WINDOW,
            payload: {
                id: id
            }
        })
    }
}

export function openMyModalWindow(id) {
    return function(dispatch) {
        dispatch({
            type: OPEN_MY_MODAL_WINDOW,
            payload: {
                id: id
            }
        })
    }
}

export function closeCommonModalWindow() {
    return function(dispatch) {
        dispatch({
            type: CLOSE_COMMON_MODAL_WINDOW
        })
    }
}

export function closeMyModalWindow() {
    return function(dispatch) {
        dispatch({
            type: CLOSE_MY_MODAL_WINDOW
        })
    }
}

export function setIdOfEditTask(id) {
    return function(dispatch) {
        dispatch({
            type: SET_ID_OF_EDIT_TASK,
            payload: {
                id: id
            }
        })
    }
}