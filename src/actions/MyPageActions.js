import { SHOW_THE_COMPLETED_TASKS, HIDE_THE_COMPLETED_TASKS} from '../constants/myPage'

export function handleShowing(doesShow) {

    if (doesShow) {
        return function(dispatch) {
            dispatch({
                type: SHOW_THE_COMPLETED_TASKS
            })
        }
    } else {
        return function(dispatch) {
            dispatch({
                type: HIDE_THE_COMPLETED_TASKS
            })
        }
    }

}
