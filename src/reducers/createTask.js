import {
    EMPTY_FIELD,
    NOT_EMPTY_FIELD,
    SET_TEXT_TITLE,
    SET_TEXT_TEXT_AREA,
    SET_PRIVATE,
    DELETE_PRIVATE,
    SET_CATEGORY,
    SET_SUBCATEGORY,
    SET_DATE_TILL,
    SET_HOURS_TO_REMIND,
    SET_TAG,
    RESET_FORM
} from '../constants/createTask'

const initialState = {
    isFieldEmpty: true,
    textTitle: '',
    textTextArea: '',
    isPrivate: false,
    category: undefined,
    subcategory: undefined,
    dateTill: '',
    hoursToRemind: 12,
    typeWordOfHours: 'часов',
    tag: 0,
    errorMessage: null
};

export default function createTask(state = initialState, action) {
    switch (action.type) {
        case EMPTY_FIELD:
            return { ...state, isFieldEmpty: true, errorMessage: action.payload.errorMessage };
        case NOT_EMPTY_FIELD:
            return { ...state, isFieldEmpty: false, errorMessage: null };
        case SET_TEXT_TITLE:
            return { ...state, textTitle: action.payload.textTitle };
        case SET_TEXT_TEXT_AREA:
            return { ...state, textTextArea: action.payload.textTextArea };
        case SET_PRIVATE:
            return { ...state, isPrivate: true };
        case DELETE_PRIVATE:
            return { ...state, isPrivate: false };
        case SET_CATEGORY:
            return { ...state, category: action.payload.category };
        case SET_SUBCATEGORY:
            return { ...state, subcategory: action.payload.subcategory };
        case SET_DATE_TILL:
            return { ...state, dateTill: action.payload.dateTill };
        case SET_HOURS_TO_REMIND:
            return { ...state, hoursToRemind: action.payload.hoursToRemind,
                typeWordOfHours: action.payload.typeWordOfHours };
        case SET_TAG:
            return { ...state, tag: action.payload.tag, errorMessage: action.payload.errorMessage };
        case RESET_FORM:
            return initialState;
        default:
            return state;
    }
}
