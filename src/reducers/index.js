import { combineReducers } from 'redux'
import nav from './nav'
import loginForm from './loginForm'
import tasks from './tasks'
import sorting from './sorting'
import createTask from './createTask'
import createCategory from './createCategory'
import myPage from './myPage'

export default combineReducers({
    nav,
    loginForm,
    tasks,
    sorting,
    createTask,
    createCategory,
    myPage
})
