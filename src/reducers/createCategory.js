import {
    EMPTY_FIELD,
    NOT_EMPTY_FIELD,
    SET_TEXT_TITLE,
    SET_TEXT_TEXT_AREA,
    SET_CATEGORY,
    SET_SUBCATEGORY,
    SET_TYPE_OF_CREATION,
    RESET_FORM
} from '../constants/createCategory'

const initialState = {
    typeOfCreation: 'category',
    isFieldEmpty: true,
    textTitle: '',
    textTextArea: '',
    category: undefined,
    subcategory: undefined,
    errorMessage: null
};

export default function createCategory(state = initialState, action) {
    switch (action.type) {
        case SET_TYPE_OF_CREATION:
            return { ...state, typeOfCreation: action.payload.typeOfCreation}
        case EMPTY_FIELD:
            return { ...state, isFieldEmpty: true, errorMessage: action.payload.errorMessage };
        case NOT_EMPTY_FIELD:
            return { ...state, isFieldEmpty: false, errorMessage: null };
        case SET_TEXT_TITLE:
            return { ...state, textTitle: action.payload.textTitle };
        case SET_TEXT_TEXT_AREA:
            return { ...state, textTextArea: action.payload.textTextArea };
        case SET_CATEGORY:
            return { ...state, category: action.payload.category };
        case SET_SUBCATEGORY:
            return { ...state, subcategory: action.payload.subcategory };
        case RESET_FORM:
            return initialState;
        default:
            return state;
    }
}
