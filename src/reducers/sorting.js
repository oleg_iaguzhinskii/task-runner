import { SORT_TASK } from '../constants/sorting'

const initialState = {
    sortBy: 'date',
    descending: false
};

export default function sorting(state = initialState, action) {
    switch (action.type) {
        case SORT_TASK:
            return { ...state, sortBy: action.payload.sortBy, descending: action.payload.descending };
        default:
            return state;
    }
}
