import {
    OPEN_COMMON_MODAL_WINDOW,
    OPEN_MY_MODAL_WINDOW,
    CLOSE_COMMON_MODAL_WINDOW,
    CLOSE_MY_MODAL_WINDOW,
    SET_ID_OF_EDIT_TASK
} from '../constants/tasks'

const initialState = {
    openedCommonModalWindow: null,
    openedMyModalWindow: null,
    idListOfCompletedTasks: null,
    id: null
};

export default function tasks(state = initialState, action) {
    switch (action.type) {
        case OPEN_COMMON_MODAL_WINDOW:
            return { ...state, openedCommonModalWindow: action.payload.id };
        case OPEN_MY_MODAL_WINDOW:
            return { ...state, openedMyModalWindow: action.payload.id };
        case CLOSE_COMMON_MODAL_WINDOW:
            return { ...state, openedCommonModalWindow: null };
        case CLOSE_MY_MODAL_WINDOW:
            return { ...state, openedMyModalWindow: null };
        case SET_ID_OF_EDIT_TASK:
            return { ...state, id: action.payload.id };
        default:
            return state;
    }
}