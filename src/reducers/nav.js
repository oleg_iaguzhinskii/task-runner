import { SET_OPENED_PAGE } from '../constants/nav'

const initialState = {
    page: '/'

};

export default function nav(state = initialState, action) {
    switch (action.type) {
        case SET_OPENED_PAGE:
            return { ...state, page: action.payload.page };
        default:
            return state;
    }
}
