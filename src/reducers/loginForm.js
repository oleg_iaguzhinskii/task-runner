import {
    LOGIN_PENDING,
    LOGIN_SUCCESS,
    LOGIN_ERROR,
    LOG_OUT,
    INVALID_LOGIN_TEXT,
    VALID_LOGIN_TEXT
} from '../constants/loginForm'

const initialState = {
    isFetching: false,
    isLogged: false,
    errorMessage: '',
    loggedUserLogin: null,
    isLoginTextInvalid: false
};

export default function loginForm(state = initialState, action) {
    switch (action.type) {
        case LOGIN_PENDING:
            return { ...state, isFetching: true, errorMessage: '' };
        case LOGIN_SUCCESS:
            return { ...state, isFetching: false, isLogged: true, loggedUserLogin: action.payload.loggedUserLogin };
        case LOGIN_ERROR:
            return { ...state, isFetching: false, errorMessage: action.payload.errorMessage };
        case LOG_OUT:
            return { ...state, isLogged: false, loggedUserLogin: null };
        case INVALID_LOGIN_TEXT:
            return { ...state, isLoginTextInvalid: true, errorMessage: action.payload.errorMessage };
        case VALID_LOGIN_TEXT:
            return { ...state, isLoginTextInvalid: false, errorMessage: action.payload.errorMessage };
        default:
            return state;
    }
}