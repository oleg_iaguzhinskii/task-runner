export default function getDate(date) {
    let yy = date.getFullYear(),
        mm = date.getMonth() + 1,
        dd = date.getDate(),
        hh = date.getHours(),
        mi = date.getMinutes();

    if (yy < 10) yy = '0' + yy;
    if (mm < 10) mm = '0' + mm;
    if (dd < 10) dd = '0' + dd;
    return yy + '-' + mm + '-' + dd + ' в ' + hh + ':' + mi;
}
