import { setFieldEmpty } from '../actions/CreateCategoryActions'

export default function autoTextEdit(text) {
    let sentences = [];
    text = text.replace(/\[.+\]/, '');

    if (text.match(/\S/) === null) {
        setFieldEmpty();
        return;
    }

    text = text.replace(/^\s+/, '').replace(/\s+$/, '')
        .replace(/\s{2,}/g, ' ');

    sentences = text.toLowerCase().match(/[^.]+\.?( *|$)/g);
    sentences = sentences.map(e => e.charAt(0).toUpperCase() + e.slice(1));

    text = sentences.join('');

    return text;
}
