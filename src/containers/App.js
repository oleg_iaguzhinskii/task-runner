import React, { Component } from 'react'

import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Navigation from '../components/Navigation'
import LoginPage from '../components/pages/LoginPage'
import CreateTaskPage from '../components/pages/CreateTaskPage'
import CreateCategoryPage from '../components/pages/CreateCategoryPage'
import CommonTasksPage from '../components/pages/CommonTasksPage'
import MyTasksPage from '../components/pages/MyTasksPage'
import MyPagePage from '../components/pages/MyPagePage'
import SettingsPage from '../components/pages/SettingsPage'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { logout, setOpenedPage } from '../actions/NavActions'
import styles from '../../styles/main.css';

class App extends Component {
    render() {
        const { isLogged, loggedUserLogin } = this.props.data;
        const { page } = this.props.nav;
        const { logout, setOpenedPage } = this.props;
        return (
            <BrowserRouter>
                <div className={styles.body}>
                    <Navigation loggedUserLogin={loggedUserLogin} isLogged={isLogged} page={page}
                         logout={logout} setOpenedPage={setOpenedPage} />
                    <Switch>
                        <Route exact path='/' component={CommonTasksPage} />
                        <Route path='/my_tasks' component={MyTasksPage} />
                        <Route path='/login' component={LoginPage} />
                        <Route path='/create_task' component={CreateTaskPage} />
                        <Route path='/create_category' component={CreateCategoryPage} />
                        <Route path='/my_page' component={MyPagePage} />
                        <Route path='/settings' component={SettingsPage} />
                    </Switch>
                </div>
            </BrowserRouter>

        )
    }
}

function mapStateToProps(state) {
    return {
        data: state.loginForm,
        nav: state.nav
    }
}

function mapDispatchToProps(dispatch) {
    return {
        logout: bindActionCreators(logout, dispatch),
        setOpenedPage: bindActionCreators(setOpenedPage, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
